///////////////////////////
///// CSE 2 HW04
///// Nicole M. Bolton
///// Virtual Yahtzee Game;  
///
//
import java.util.Scanner;

public class Yahtzee{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    
    int rollOne = 0, rollTwo = 0, rollThree = 0, rollFour = 0, rollFive = 0; //Five Dice Rolls
    
    System.out.print("Welcome to Yahtzee! \n\nPress 1 To Input Own Numbers or Press 2 To Roll Randomly: "); //Give user option to input numbers or randomly roll
    int userDecision = myScanner.nextInt(); // Accept user input for decision
    
    //If user inputs a valid decision
    if (userDecision == 1 || userDecision == 2){
      //Presses 1: User Inputs Own Numbers Via 5 Digit Number
      if (userDecision == 1){
      System.out.print("\nInput a 5 Digit Number: "); //Prompt User to Input own number
        int userNumber = myScanner.nextInt(); //Accept user input for five digit number
        
        //Check if user number is a valid 5 digit number
        if (userNumber < 11111 || userNumber > 66666 ){
          System.out.println("Invalid Roll");
          System.exit(0);
        }
        
        //Seperate 5 digit number into single rolls
        rollOne = ((userNumber % 100000) - (userNumber % 10000)) / 10000; //Single Digit For 10000th Place in the 5 Digit Number
        rollTwo = ((userNumber % 10000) - (userNumber % 1000)) / 1000; //Single Digit For 1000th Place in the 5 Digit Number
        rollThree = ((userNumber % 1000) - (userNumber % 100)) / 100; //Single Digit For 100th Place in the 5 Digit Number
        rollFour = ((userNumber % 100) - (userNumber % 10)) / 10; //Single Digit For 10th Place in the 5 Digit Number
        rollFive = (userNumber % 10) - (userNumber % 1); //Single Digit For 1st Place in the 5 Digit Number
        
        //Check that each roll is between 1 - 6
        if (rollOne < 1 || rollOne > 6){
          System.out.println("Invalid Roll");
          System.exit(0);
        }
        if (rollTwo < 1 || rollTwo > 6){
          System.out.println("Invalid Roll");
          System.exit(0);
        }
        if (rollThree < 1 || rollThree > 6){
          System.out.println("Invalid Roll");
          System.exit(0);
        }
        if (rollFour < 1 || rollFour > 6){
          System.out.println("Invalid Roll");
          System.exit(0);
        }
        if (rollFive < 1 || rollFive > 6){
          System.out.println("Invalid Roll");
          System.exit(0);
        }
      }
    
      //Presses 2: Randomly Generated Numbers
      if (userDecision == 2){
        rollOne = (int)(Math.random() * 6 + 1); //1 Random Roll from 1 - 6
        rollTwo = (int)(Math.random() * 6 + 1); //2 Random Roll from 1 - 6
        rollThree = (int)(Math.random() * 6 + 1); //3 Random Roll from 1 - 6
        rollFour = (int)(Math.random() * 6 + 1); //4 Random Roll from 1 - 6
        rollFive = (int)(Math.random() * 6 + 1); //5 Random Roll from 1 - 6
      }
      
      //Adding the Groups/Categories of Similar Rolls
      int Aces = 0, Twos = 0, Threes = 0, Fours = 0, Fives = 0, Sixes = 0;
     
      //Roll One
      switch (rollOne){
        case 1: //If Roll One = 1, Add to Aces
          Aces = Aces + 1;
          break;
        case 2: //If Roll One = 2, Add to Twos
          Twos = Twos + 2;
          break;
        case 3: //If Roll One = 3, Add to Threes
          Threes = Threes + 3;
          break;
        case 4: //If Roll One = 4, Add to Fours
          Fours = Fours + 4;
          break;
        case 5: //If Roll One = 5, Add to Fives
          Fives = Fives + 5;
          break;
        case 6: //If Roll One = 6, Add to Sixes
          Sixes = Sixes + 6;
          break;
      }  
      
       //Roll Two
      switch (rollTwo){
        case 1: //If Roll Twos = 1, Add to Aces
          Aces = Aces + 1;
          break;
        case 2: //If Roll Twos = 2, Add to Twos
          Twos = Twos + 2;
          break;
        case 3: //If Roll Twos = 3, Add to Threes
          Threes = Threes + 3;
          break;
        case 4: //If Roll Twos = 4, Add to Fours
          Fours = Fours + 4;
          break;
        case 5: //If Roll Twos = 5, Add to Fives
          Fives = Fives + 5;
          break;
        case 6: //If Roll Twos = 6, Add to Sixes
          Sixes = Sixes + 6;
          break;
      }
      
       //Roll Three
      switch (rollThree){
        case 1: //If Roll Three = 1, Add to Aces
          Aces = Aces + 1;
          break;
        case 2: //If Roll Three = 2, Add to Twos
          Twos = Twos + 2;
          break;
        case 3: //If Roll Three = 3, Add to Threes
          Threes = Threes + 3;
          break;
        case 4: //If Roll Three = 4, Add to Fours
          Fours = Fours + 4;
          break;
        case 5: //If Roll Three = 5, Add to Fives
          Fives = Fives + 5;
          break;
        case 6: //If Roll Three = 6, Add to Sixes
          Sixes = Sixes + 6;
          break;
      }
      
      //Roll Four
      switch (rollFour){
        case 1: //If Roll Four = 1, Add to Aces
          Aces = Aces + 1;
          break;
        case 2: //If Roll Four = 2, Add to Twos
          Twos = Twos + 2;
          break;
        case 3: //If Roll Four = 3, Add to Threes
          Threes = Threes + 3;
          break;
        case 4: //If Roll Four = 4, Add to Fours
          Fours = Fours + 4;
          break;
        case 5: //If Roll Four = 5, Add to Fives
          Fives = Fives + 5;
          break;
        case 6: //If Roll Four = 6, Add to Sixes
          Sixes = Sixes + 6;
          break;
      }
      
      //Roll Five
      switch (rollFive){
        case 1: //If Roll Five = 1, Add to Aces
          Aces = Aces + 1;
          break;
        case 2: //If Roll Five = 2, Add to Twos
          Twos = Twos + 2;
          break;
        case 3: //If Roll Five = 3, Add to Threes
          Threes = Threes + 3;
          break;
        case 4: //If Roll Five = 4, Add to Fours
          Fours = Fours + 4;
          break;
        case 5: //If Roll Five = 5, Add to Fives
          Fives = Fives + 5;
          break;
        case 6: //If Roll Five = 6, Add to Sixes
          Sixes = Sixes + 6;
          break;  
      }
      
      //Print Statements to Check if Points Added Properly
      //System.out.println("Aces:   " + Aces); //Total of Aces
      //System.out.println("Twos:   " + Twos); //Total of Twos
      //System.out.println("Threes: " + Threes); //Total of Threes
      //System.out.println("Fours:  " + Fours); //Total of Fours
      //System.out.println("Fives:  " + Fives); //Total of Fives
      //System.out.println("Sixes:  " + Sixes); //Total of Sixes
      
      //CHECKING FOR SPECIAL PROPERTIES OF ROLLS
      
      //THREE OF A KIND
      
      //Check if rolls have three of a kind
      //If three dice are identical; the sorted categories of rolls will be = roll value * 3
      //If there is a three of a kind, value is added to variable threeOfKind so it can be added at the end to the totals
      //Check to make sure that the three of a kind is not a full house
      int threeOfKind = 0;
      
      if (Aces == 3 && (Twos != 4 && Threes != 6 && Fours != 8 && Fives != 10 && Sixes != 12)){
        threeOfKind = 3;
      }
      if (Twos == 6 && (Aces != 2 && Threes != 6 && Fours != 8 && Fives != 10 && Sixes != 12)){
        threeOfKind = 6;
      }
      if (Threes == 9 && (Aces != 2 && Twos != 4 && Fours != 8 && Fives != 10 && Sixes != 12)){
        threeOfKind = 9;
      }
      if (Fours == 12 && (Aces != 2 && Twos != 4 && Threes != 6 && Fives != 10 && Sixes != 12)){
        threeOfKind = 12;
      }
      if (Fives == 15 && (Aces != 2 && Twos != 4 && Threes != 6 && Fours != 8 && Sixes != 12)){
        threeOfKind = 15;
      }
      if (Sixes == 18 && (Aces != 2 && Twos != 4 && Threes != 6 && Fours != 8 && Fives != 10)){
        threeOfKind = 18;
      }
      
      //FOUR OF A KIND
      
      //Check if rolls have four of a kind
      //If four dice are identical; the sorted categories of rolls will be = roll value * 4
      //If there is a four of a kind, value is added to variable fourOfKind so it can be added at the end to the totals
      int fourOfKind = 0;
      
      if (Aces == 4){
        fourOfKind = 4;
      }
      if (Twos == 8){
        fourOfKind = 8;
      }
      if (Threes == 12){
        fourOfKind = 12;
      }
      if (Fours == 16){
        fourOfKind = 16;
      }
      if (Fives == 20){
        fourOfKind = 20;
      }
      if (Sixes == 24){
        fourOfKind = 24;
      }
      
      //YAHTZEE
      
      //Check if rolls have 5 of a kind aka YAHTZEE
      //If five dice are identical; the sorted categories of rolls = roll value * 5
      //Get 50 points for a YAHTZEE
      int Yahtzee = 0;
      if (Aces == 5 || Twos == 10 || Threes == 15 || Fours == 20 || Fives == 25 || Sixes == 30){
        Yahtzee = 50;
      }
      //FULL HOUSE
      
      //Check if rolls have a full house
      //If rolls have a full house one category of roll will be a multiple of three and another will be a multiple of two
      //Get 25 Points for a fullhouse
      
      int fullHouse = 0;
      if (Aces == 3 && (Twos == 4 || Threes == 6 || Fours == 8 || Fives == 10 || Sixes == 12)){ //Three Aces and Two of any other kind
        fullHouse = 25;
      }
      if (Twos == 6 && (Aces == 2 || Threes == 6 || Fours == 8 || Fives == 10 || Sixes == 12)){ //Three Twos and Two of any other kind
        fullHouse = 25;
      }
      if (Threes == 9 && (Aces == 2 || Twos == 4 || Fours == 8 || Fives == 10 || Sixes == 12)){ //Three Threes and Two of any other kind
        fullHouse = 25;
      }
      if (Fours == 12 && (Aces == 2 || Twos == 4 || Threes == 6 || Fives == 10 || Sixes == 12)){ //Three Fours and Two of any other kind
        fullHouse = 25;
      }
      if (Fives == 15 && (Aces == 2 || Twos == 4 || Threes == 6 || Fours == 8 || Sixes == 12)){ //Three Fives and Two of any other kind
        fullHouse = 25;
      }
      if(Sixes == 18 && (Aces == 2 || Twos == 4 || Threes == 6 || Fours == 8 || Fives == 10)){ //Three Sixes and Two of any other kind
        fullHouse = 25;
      }
      
      //Check if rolls are a short straight
      //If 4 sequential categories of roll have one multiple in it, then it is a short straight
      //Get 30 points for a short straight
      int shortStraight = 0;
      if (Aces == 1 && Twos == 2 && Threes == 3 && Fours == 4 && Fives != 5){
        shortStraight = 30;
      }
      if (Twos == 2 && Threes == 3 && Fours == 4 && Fives == 5 && Aces != 1 && Sixes != 6){
        shortStraight = 30;
      }
      if (Threes == 3 && Fours == 4 && Fives == 5 && Sixes == 6 && Twos != 2){
        shortStraight = 30;
      }
      
      //Check if rolls are a long straight
      //If 5 sequential categories of roll have one multiple in it, then it is a long straight
      //Get 40 points for a long straight
      int longStraight = 0;
      if (Aces == 1 && Twos == 2 && Threes == 3 && Fours == 4 && Fives == 5){
        longStraight = 40;
      }
      if (Twos == 2 && Threes == 3 && Fours == 4 && Fives == 5 && Sixes == 6){
        longStraight = 40;
      }
      
      //Calculate the Chance value = sum of all 5 dice
      //Only Calculate Chance if roll doesn't apply for any of the other properties or if it is gives a greater calculated value then the other properties
      int Chance = rollOne + rollTwo + rollThree + rollFour + rollFive;
        
      //Print Statements to Check if Points Added Properly
      //System.out.println("Three of a Kind Points: " + threeOfKind);
      //System.out.println("Four of a Kind Points: " + fourOfKind);
      //System.out.println("Yahtzee Points: " + Yahtzee);
      //System.out.println("Short Straight Points: " + shortStraight);
      //System.out.println("Long Straight Points: " + longStraight);
      //System.out.println("Full House Points: " + fullHouse);
      //System.out.println("Chance Points: " + Chance);
      
      
      //UPPER SECTION TOTAL
      
      //Calculate Upper Total
      int upperTotal = Aces + Twos + Threes + Fours + Fives + Sixes;
      System.out.println("Upper Section Initial Total:  " + upperTotal);
     
      //Check if value of total applies for a bonus
      int upperTotalBonus = upperTotal;
      if (upperTotal >= 63){
        upperTotalBonus = upperTotal + 35;
      }
      System.out.println("Upper Section Total With Bonus: " + upperTotalBonus);
      
      
      //LOWER SECTION TOTAL
      
      //Calculate Lower Total
      int lowerTotal = threeOfKind + fourOfKind + Yahtzee + fullHouse + shortStraight + longStraight + Chance;
      System.out.println("Lower Section Total: " + lowerTotal);
     
        
      //GRAND TOTAL
      
      //Calculate Grand Total
      int grandTotal = upperTotalBonus + lowerTotal;
      System.out.println("Grand Total: " + grandTotal);
        
    }
    
    //User does not input 1 or 2
    //if(userDecision != 1 && userDecision != 2){
    else{
      System.out.println("Error, Please Input 1 or 2");
      System.exit(0);
    }
  }
}
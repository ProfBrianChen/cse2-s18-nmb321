///////////////////////////
/// Nicole M. Bolton
/// CSE 2 Homework 09 -- DrawPoker
/// Generate two poker hands and decide which hand wins

import java.util.Random; //Random Num Generator

public class DrawPoker{
  
  //MAIN METHOD//
  public static void main(String[] args){
    
    //Arrays For Two Hands
    int[] hand1 = new int[5];
    int[] hand2 = new int[5];
    
    //Array For Card Deck
    int[] deck = new int[52];
    //int[] shuffle = new int[52];
    
    //Make Card Deck
    for(int i = 0; i < deck.length; i++){
      deck[i] = i;
    }
    
    //Shuffle Card Deck
    for(int j = 0; j < deck.length; j++ ){
      int target = (int) (deck.length * Math.random() ); //Random Value to Switch
      //swap the values
	    int temp = deck[target];
	    deck[target] = deck[j];
	    deck[j] = temp;
    }
    
    //Loop to Distribute Cards
    int n = 0;
    for(int k = 0; k < 5; k ++){
      hand1[k] = deck[n];
      hand2[k] = deck[n+1];
      n += 2;
    }
    
    //Go to Method to Shuffle to Hands
    //generateHands(hand1, hand2);
    
    //int[] hand1 = new int[]{0,1,2,3,4};
    //int[] hand2 = new int[]{13, 26, 39, 11, 24};
    
    //Convert Card Numbers into Names
    String[] hand1WithNames = cardNames(hand1);
    String[] hand2WithNames = cardNames(hand2);
    
    //Print Out First Hand
    System.out.print("First Hand: ");
    printArray(hand1WithNames);
    
    //Print Out Second Hand
    System.out.print("Second Hand: ");
    printArray(hand2WithNames);
    
    //Determine if Hands Satisfy Any Poker Combination
    //Written in Order of Natural Ordering of Hands, Breaks if Satifies One of The Conditions. 
    if(fullHouse(hand1) && !fullHouse(hand2)){
      System.out.println("First Hand Wins!");
      return;
    }
    
    else if(fullHouse(hand2) && !fullHouse(hand1)){
      System.out.println("Second Hand Wins!");
      return;
    }
    
    else if(fullHouse(hand1) && fullHouse(hand2)){
      System.out.println("Tie!");
      return;
    }
    
    else if(flush(hand1) && !flush(hand2)){
      System.out.println("First Hand Wins!");
      return;
    }
    
    else if(flush(hand2) && !flush(hand1)){
      System.out.println("Second Hand Wins!");
      return;
    }
    
    else if(flush(hand1) && flush(hand2)){
      System.out.println("Tie!");
      return;
    }
    
    else if(threeOfKind(hand1) && !threeOfKind(hand2)){
      System.out.println("First Hand Wins!");
      return;
    }
    
    else if(threeOfKind(hand2) && !threeOfKind(hand1)){
      System.out.println("Second Hand Wins!");
      return;
    }
    
    else if(threeOfKind(hand1) && threeOfKind(hand2)){
      System.out.println("Tie!");
      return;
    }
    
    else if(pair(hand1) && !pair(hand2)){
      System.out.println("First Hand Wins!");
      return;
    }
    
    else if(pair(hand2) && !pair(hand1)){
      System.out.println("Second Hand Wins!");
      return;
    }
    
    else if(pair(hand1) && pair(hand2)){
      System.out.println("Tie!");
      return;
    }
    
    //If None of the Hands Satisfy Anything, Find the Highest Card
    else{
      int highestCard1 = 0;
      int highestCard2 = 0;
      
      for(int i = 0; i < hand1.length; i ++){
        if(hand1[i]%13 > highestCard1){
          highestCard1 = hand1[i]%13;
        }
      }
      
      for(int j = 0; j < hand2.length; j ++){
        if(hand2[j]%13 > highestCard2){
          highestCard2 = hand2[j]%13;
        }
      }
      
      if(highestCard1 > highestCard2){
        System.out.println("First Hand Wins!");
        
      }
      if(highestCard2 > highestCard1){
        System.out.println("Second Hand Wins!");
      }
      if(highestCard1 == highestCard2){
        System.out.println("Tie!");
      }
    }
    
    /*System.out.println(pair(hand1));
    System.out.println(pair(hand2));
    
    System.out.println(threeOfKind(hand1));
    System.out.println(threeOfKind(hand2));
    
    System.out.println(flush(hand1));
    System.out.println(flush(hand2));
    
    System.out.println(fullHouse(hand1));
    System.out.println(fullHouse(hand2));*/
    
  }
  
  //METHOD FOR GENERATING PLAY HANDS//
  /*public static void generateHands(int[] h1, int[] h2){
    
    Random rand = new Random(); //random generator
    for(int i = 0; i < 5; i ++){ //loop for all indexes in both arrays, draw 5 cards
      
      int randCard1 = rand.nextInt(51); //random number for card in hand 1
      
      
      while(inArray(randCard1, h1, h2)){ //go to method inArray(), check if the number for the card has already been drawn in either hand
        randCard1 = rand.nextInt(51); //if already drawn, loop until card that hasn't been drawn is drawn
      }
      
      h1[i] = randCard1; //add card to hand if a valid number 
      
      int randCard2 = rand.nextInt(51); //random number for card in hand 2

      while(inArray(randCard2, h1, h2)){ //go to method inArray(), check if the number for the card has already been drawn in either hand
        randCard2 = rand.nextInt(51); //if already drawn, loop until card that hasn't been drawn is drawn
      }
      
      h2[i] = randCard2; //add card to hand if a valid number 
    }
  }*/
  
  //METHOD TO DETERMINE IF A CARD HAS ALREADY BEEN DRAWN//
  /*public static boolean inArray(int x, int[] a1, int[] a2){
    
    boolean boolInArray = true; 
    
    for(int i = 0; i < 5; i++){ //loop through all indexes of the two hands
      
      if(a1[i] == x){ //if the card is in hand one
        boolInArray = true; //set boolean to true, this way the while loop in generateHands will continue to loop
        break; //break from loop
      }
      else if(a2[i] == x){ //if the card is in hand two
        boolInArray = true; //set boolean to true, this way the while loop in generateHands will continue to loop
        break; //break from loop
      }
      else if(x == 0){ //if the random card is zero and zero has yet to be selected, exit loop, otherwise it will be equal to the rest of the array b/c the blank values are one
        boolInArray = false; //return false b/c the number hasn't been selected yet
        break; //break from loop
      }
      else{ //random card not found at current index
        boolInArray = false; //set boolean to false and reloop for next index
      }
    }
    return boolInArray; //return boolean
  }*/

  //METHOD TO DETERMINE IF HAND HAS A PAIR//
  public static boolean pair(int[] a){
    
    boolean boolPair = false; //Boolean for Pair 
    int pairCnt = 0; //Counter for Pair
    
    for(int i = 0; i < a.length; i ++){
      int j = 0; 
      while(i > j){ //So Numbers Don't get Counted Twice Compares Current Index to All Numbers that Came Before It 
        if(a[i]%13 == a[j]%13){ //If Modulus Are Equal, Have Same Card Value
          pairCnt++; //Keep Count of Pairs
        }
        j++;
      }
    }
    if(pairCnt == 1 || pairCnt == 2){ //If Count at End Equals 1 Then There Was a Single Pair, If Count at End Equals 2 Then There Were Two Pairs
      boolPair = true;
    }
    return boolPair;
  }
  
  //METHOD TO DETERMINE IF HAND HAS THREE OF A KIND//
  public static boolean threeOfKind(int[] a){
    
    boolean boolThreeOfKind = false;
    int threeCnt = 0;
    
    for(int i = 0; i < a.length; i++){
      int j = 0;
      while(i > j){ //So Numbers Don't get Counted Twice Compares Current Index to All Numbers that Came Before It 
        if(a[i]%13 == a[j]%13){ //If Modulus Are Equal, Have Same Card Value
          threeCnt++; //Keep Count of Triples
        }
        j++;
      }
    }
    if(threeCnt == 3){ //If Count at End Equals 3 Then There Was a Three of a Kind
      boolThreeOfKind = true;
    }
    return boolThreeOfKind; 
  }
  
  //METHOD TO DETEMINE IF HAND HAS A FLUSH//
  public static boolean flush(int[] a){
    
    boolean boolFlush = false;
    int flushCnt = 0;
    
    for(int i = 1; i < a.length; i++){
        if(a[0]/13 == a[i]/13){ //Only Need to Compare One Value to All the Other to Determine a Flush, If i/13 are equal, have same suit
          flushCnt++;
        }
      }
    if(flushCnt == 4){ //If Count Equals 4, There is a flush
      boolFlush = true;
    }
    return boolFlush;
  }
  
  //METHOD TO DETERMINE IF HAND HAS A FULL HOUSE//
  public static boolean fullHouse(int[] a){
    
    boolean boolFullHouse = false;
    int fullHouseCnt = 0;
    
    for(int i = 0; i < a.length; i++){
      int j = 0;
      while(i > j){ //So Numbers Don't get Counted Twice Compares Current Index to All Numbers that Came Before It
        if(a[i]%13 == a[j]%13){ //If Modulus Are Equal, Have Same Card Value
          fullHouseCnt++;
        }
        j++;
      }
    }
    if(fullHouseCnt == 4){ //If Count Equals 4, There is a Full House
      boolFullHouse = true;
    }
    return boolFullHouse;
  }
  
  
  //METHOD TO CONVERT CARD NAMES// (Wasn't Necessary, Did For Convenience)
  public static String[] cardNames(int[] a){
    
    String cardNames[] = new String[a.length]; //create a new array
    String suit = "";
    String value = "";
    
    for(int i = 0; i < a.length; i++){ //loop through the array
      
      //if statements to determine cards suit, the result of the a[i]/13 determines the suit
      if(a[i]/13 == 0){
         suit = "Diamonds";
      }
      if(a[i]/13 == 1){
         suit = "Clubs";
      }
      if(a[i]/13 == 2){
         suit = "Hearts";
      }
      if(a[i]/13 == 3){
         suit = "Spades";
      }
      
      //switch statements to determine value of card, the result of a[i]%13 determines the value of card 
      switch(a[i]%13) {
        case 0: value = "Ace";
          break;
        case 1: value = "2";
          break;
        case 2: value = "3";
          break;
        case 3: value = "4";
          break;
        case 4: value = "5";
          break;
        case 5: value = "6";
          break;
        case 6: value = "7";
          break;
        case 7: value = "8";
          break;
        case 8: value = "9";
          break;
        case 9: value = "10";
          break;
        case 10: value = "Jack";
          break;
        case 11: value = "Queen";
          break;
        case 12: value = "King";
          break;
      }
      
      cardNames[i] = ( value + " of " + suit); //make the value in the array equal to the card name
    }
    return cardNames; //return the new array with card names 
  }
  
  
  //METHOD TO PRINT NAMES OF CARDS IN HAND//
  public static void printArray(String[] a){
    for(int i = 0; i < a.length; i ++){
      System.out.print(a[i]);
      if(i < a.length - 1){
        System.out.print(", ");
      }
    }
    System.out.println();
  }
}
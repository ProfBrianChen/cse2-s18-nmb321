//////////////////////////
///// CSE 2 Lab10
///// Nicole M. Bolton
import java.util.Random;

public class lab10{
  public static void main(String args[]){
    Random rand = new Random();
    int height1 = rand.nextInt(5)+1;
    int width1 = rand.nextInt(5)+1;
    int height2 = rand.nextInt(5)+1;
    int width2 = rand.nextInt(5)+1;
    
    boolean boolean1 = true;
    boolean boolean2 = false;
    
    int[][] A = increasingMatrix(height1, width1, boolean1);
    int[][] B = increasingMatrix(height1, width1, boolean2);
    int[][] C = increasingMatrix(height2, width2, boolean1);
    
    System.out.println("Generating a matrix with width " + width1 + " and height " + height1 + ":");
    printMatrix(A, boolean1);
    System.out.println("Generating a matrix with width " + width1 + " and height " + height1 + ":");
    printMatrix(B, boolean2);
    System.out.println("Generating a matrix with width " + width2 + " and height " + height2 + ":");
    printMatrix(C, boolean1);
    
    matrixAddition(A, boolean1, B, boolean2);
    
    matrixAddition(A, boolean1, C, boolean1);
   
  }
  
  public static int[][] increasingMatrix(int height, int width, boolean Format){
    
    //Row Major//
    int count = 1;
    if (Format) { // generate row-major matrix
      int[][] array = new int[height][width]; 
      for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
          array[i][j] = count;
          count++;
        }
      }
      return array;
    }
    //Column Major//
    else {
      int[][] array = new int[height][width]; 
      for (int j = 0; j < width; j++) {
        for (int i = 0; i < height; i++) {
          array[i][j] = count;
          count++;
        }
      }
      return array;
    }
  }
  
  public static void printMatrix(int[][] array, boolean Format){
    if(array == null){
      System.out.print("the array was empty!");
      return;
    }
    if(Format){
      for (int i = 0; i < array.length; i++) {
        System.out.print("[");
        for (int j = 0; j < array[0].length; j++) {
          System.out.print(" " + array[i][j]);
        }
        System.out.print(" ]");
        System.out.println();
      }
    }
    else{
      for (int j = 0; j < array.length; j++) {
        System.out.print("[");
        for (int i = 0; i < array[0].length; i++) {
          System.out.print(" " + array[j][i]);
        }
        System.out.print(" ]");
        System.out.println();
      }
    }
  }
  
  public static void matrixAddition(int[][] a, boolean formata, int[][] b, boolean formatb){
    if(a.length == b.length && a[0].length == b[0].length){
      System.out.println("Adding two matrices.");
      printMatrix(a, formata);
      System.out.println("and");
      printMatrix(b, formatb);
      System.out.println("output: ");
      int[][] add = new int[a.length][a[0].length];
       for (int i = 0; i < a.length; i++) {
        for (int j = 0; j < a[0].length; j++) {
          add[i][j] = a[i][j] + b[i][j];
        }
      }
      printMatrix(add, true);
      return;
    }
    else{
      System.out.println("arrays cannot be added");
      return;
    }
  }
  
}
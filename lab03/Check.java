//////////////////////////
///// CSE 2 Lab03
///// Nicole Bolton
///// Check Calculator and Divider
///
import java.util.Scanner;
//
//
//
public class Check{
  // main method required for every Java program
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in ); //Input from STDIN
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //Prompt user to input orignial cost of the check
    double checkCost = myScanner.nextDouble(); //Accept user input for cost of check
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // Prompt user for the tip percentage
    double tipPercent = myScanner.nextDouble(); //Accept user input for tip percentage
    tipPercent /= 100; //Want to convert the percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); //Prompt user input for the number of people
    int numPeople = myScanner.nextInt(); //Accept user input for number of people
    
    double totalCost;
    double costPerPerson;
    int dollars, //whole dollar amount of cost
          dimes, pennies; ///for storing digits to the right of the decimal poin for the cost
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    //get dimes amount, e.g.,
    // (int)(6.73 * 10) % 10 --> 67 % 10 --> 7
    // where the % (mod) operator returns the remainder
    // after the division: 583 % 100 --> 83, 27 % 5 --> 2
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  } // end of main method
} // end of class
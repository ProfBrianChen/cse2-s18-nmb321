///////////////////////////
///Nicole M. Bolton
///CSE 2 Homework 06
///

import java.util.Scanner;

public class Argyle{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    int winWidth = 0; //Integer for Window Width
    int winHeight = 0; //Integer for Window Height
    int diWidth = 0; //Integer for the Argyle Diamond Width
    int strWidth = 0; //Integer for the Center Argyle Stripe Width
    String firstFill = ""; //Character for the First Pattern Fill
    String secondFill = ""; //Character for the Second Pattern Fill
    String thirdFill = ""; //Characted for the Third Pattern Fill
    
    /////////
    //INPUT//
    /////////
    
    //VARIABLE CHECK FOR WINDOW WIDTH 
    do{
      System.out.print("Please Enter a Positive Integer for the Width of Viewing Window of Characters: "); //Prompt User for winWidth
      while (!myScanner.hasNextInt()){
        myScanner.nextLine(); // Clear Scanner if User Doesn't Input an Integer
        System.out.print("Please Enter a Positive Integer for the Width of Viewing Window of Characters: ");
      }
      winWidth = myScanner.nextInt(); //If User Enters an Int
    } while (!(winWidth > 0)); //Check if Input is positive
    
    //VARIABLE CHECK FOR WINDOW HEIGHT 
    do{
      System.out.print("Please Enter a Positive Integer for the Height of Viewing Window of Characters: "); //Prompt User for winHeight
      while (!myScanner.hasNextInt()){
        myScanner.nextLine(); // Clear Scanner if User Doesn't Input an Integer
        System.out.print("Please Enter a Positive Integer for the Height of Viewing Window of Characters: ");
      }
      winHeight = myScanner.nextInt(); //If User Enters an Int
    } while (!(winHeight > 0)); //Check if Input is positive
    
    //VARIABLE CHECK FOR ARGYLE DIAMOND WIDTH
    do{
      System.out.print("Please Enter a Positive Integer for the Width of the Argyle Diamonds: "); //Prompt User for diWidth
      while (!myScanner.hasNextInt()){
        myScanner.nextLine(); // Clear Scanner if User Doesn't Input an Integer
        System.out.print("Please Enter a Positive Integer for the Width of the Argyle Diamonds: ");
      }
      diWidth = myScanner.nextInt(); //If User Enters an Int
    } while (!(diWidth > 0)); //Check if Input is positive
    
    //VARIABLE CHECK FOR ARGYLE STRIPE WIDTH 
    do{
      System.out.print("Please Enter a Positive Odd Integer for the Width of the Argyle Center Stripe: "); //Prompt User for strWidth
      while (!myScanner.hasNextInt()){
        myScanner.nextLine(); // Clear Scanner if User Doesn't Input an Integer
        System.out.print("Please Enter a Positive Odd Integer for the Width of the Argyle Center Stripe: ");
      }
      strWidth = myScanner.nextInt(); //If User Enters an Int
    } while (!(strWidth > 0 && strWidth % 2 == 1)); //Check if Input is an odd positive
    
    //FIRST CHARACTER OF PATTERN
    String blank1 = myScanner.nextLine(); //Clear Scanner 
    System.out.print("Please Enter a First Character for Pattern Fill: "); //Prompt User for firstChar
    firstFill = myScanner.next(); //Accept User Input for the String
    char firstChar = firstFill.charAt(0); //Take First Character of the String for the Pattern Fill
    
    //SECOND CHARACTER OF PATTERN
    String blank2 = myScanner.nextLine(); //Clear Scanner 
    System.out.print("Please Enter a Second Character for Pattern Fill: "); //Prompt User for secondChar
    secondFill = myScanner.next(); //Accept User Input for the String
    char secondChar = secondFill.charAt(0); //Take First Character of the String for the Pattern Fill
    
    //THIRD CHARACTER OF PATTERN
    String blank3 = myScanner.nextLine(); //Clear Scanner 
    System.out.print("Please Enter a Third Character for Stripe Fill: "); //Prompt User for thirdChar
    thirdFill = myScanner.next(); //Accept User Input for the String
    char thirdChar = thirdFill.charAt(0); //Take First Character of the String for the Pattern Fill
    
    //System.out.println(winWidth);
    //System.out.println(winHeight);
    //System.out.println(diWidth);
    //System.out.println(strWidth);
    //System.out.println(firstChar);
    //System.out.println(secondChar);
    //System.out.println(thirdChar);
    
    //////////
    //OUTPUT//
    //////////
    
    //DIMENSIONS
    int patternSize = 2 * diWidth; //Double the Width = Total Diamond Dimension
    int stripe = strWidth / 2; //length of stripe from center
    int x = stripe; //For One Direction of Stripe
    int y = patternSize - 1; //For Opposite Direction of Stripe
    
    //LOOP FOR PATTERN
    for(int i = 0; i < winHeight; i ++){ //Height of Window
      for(int j = 0; j < winWidth; j ++){ //Length of Window
        
        //CALCULATIONS
        int modulusDif = (j % patternSize) - (i % patternSize); //Difference Between the Remainders When Divided by Total Diamond Dimension
        int modulusDif2 = (j % diWidth) - (i % diWidth); //Difference Between the Remainders When Divided by One Square of Diamond Dimension
        int modulusSum = (j % patternSize) + (i % patternSize); //Sum of the Remainders When Divided by Total Diamond Dimension
        int modulusSum2 = (j % diWidth) + (i % diWidth); //Sum of the Remainders When Divided by One Square of Diamond Dimension
        int center = diWidth; //Center of Patten Square
        
        //BOOLEAN FOR STRIPE
        boolean modulusDifBool = (modulusDif >= -x && modulusDif <= x); 
        boolean modulusSumBool = (modulusSum >= y-x && modulusSum <= y+x);
        
        //LOOPS FOR STRIPE
        while(modulusDifBool){
          System.out.print(thirdChar);
          break;
        }
        while(modulusSumBool && !modulusDifBool){
          System.out.print(thirdChar);
          break;
        }
        
        //LOOPS FOR EVERYTHING NOT STRIPE
        while(!modulusSumBool && !modulusDifBool){
          //IF STATEMENTS FOR CENTER DIAMOND FILL
         if((j % patternSize) < center && (i % patternSize) < center && (modulusSum > diWidth -1)){ //First Quad Restrictions
           System.out.print(secondChar);
          }
          else if((j % patternSize) >= center && (i % patternSize) < center && (modulusDif < diWidth)){ //Second Quad Restrictions 
            System.out.print(secondChar);
          }
          else if((j % patternSize) >= center && (i % patternSize) >= center && (modulusSum2 < diWidth)){ //Fourth Quad Restrictions
            System.out.print(secondChar);
          }
          else if((j % patternSize) < center && (i % patternSize) >= center && (modulusDif2 >= 0)){ //Third Quad Resrtrictions
            System.out.print(secondChar);
         }
          //ELSE FOR OTHER FILL
         else{
             System.out.print(firstChar);
         }
         break;   
          }
      }
      System.out.println();
    }
  }
}
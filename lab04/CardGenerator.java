//////////////////////////
///// CSE 2 Lab04
///// Nicole M. Bolton
///// Pick Random Card from a Deck
///
//
public class CardGenerator{
  
  public static void main(String[] args){
    
    int card = (int)(Math.random() * 52 + 1);
    String suit = ""; //Open String for the suit
    String face = ""; //Open String for the face value
    
    //Determine Card Suit
    if (card >= 1 && card < 14){ //Range for Diamonds
      suit = "Diamonds";
    }
    if (card >= 14 && card < 27){ //Range for Clubs
      suit = "Clubs";
    }
    if (card >= 27 && card < 40){ //Range for Hearts
      suit = "Hearts";
    }
    if (card >= 40 && card < 53){ //Range for Spades
      suit = "Spades";
    }
    
    //Determine Card Numbers
    if (card%13 == 1){ //Modulus for Ace
      face = "Ace";
    }
    if (card%13 == 2){ //Modulus for Card Number 1
      face = "1";
    }
    if (card%13 == 3){ //Modulus for Card Number 2
      face = "2";
    }
    if (card%13 == 4){ //Modulus for Card Number 3
      face = "3";
    }
    if (card%13 == 5){ //Modulus for Card Number 4
      face = "4";
    }
    if (card%13 == 6){ //Modulus for Card Number 5
      face = "5";
    }
    if (card%13 == 7){ //Modulus for Card Number 6
      face = "6";
    }
    if (card%13 == 8){ //Modulus for Card Number 7
      face = "7";
    }
    if (card%13 == 9){ //Modulus for Card Number 8
      face = "8";
    }
    if (card%13 == 10){ //Modulus for Card Number 9
      face = "9";
    }
    if (card%13 == 11){ //Modulus for the Jack
      face = "Jack";
    }
    if (card%13 == 12){ //Modulus for the Queen
      face = "Queen";
    }
    if (card%13 == 0){ //Modulus for the King
      face = "King";
    }
    //Print out randomly generated card
    System.out.println("You picked the " + face + " of " + suit);
  }
}
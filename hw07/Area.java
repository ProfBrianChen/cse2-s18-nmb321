///////////////////////////
///Nicole M. Bolton
///CSE 2 Homework 07
///Calculate the Area of a User Selected Shape Using Multiple Methods
//

import java.util.Scanner;

public class Area{
  public static void main(String[] args){ //Main Method
    
    String shape = ""; //String for User Selection of Shape
    double Area = 0; //Double for Calculated Area
    
    Scanner myScanner = new Scanner(System.in); //Scanner
    
    System.out.print("Please Enter A Shape to Calculate Area - Rectangle, Triangle, or Circle: "); //Prompt User for what kind of shape area they want to calculate
    shape = myScanner.nextLine(); //Accept User Input as a String for Type of Shape
    shape = shape.toUpperCase(); //Change all characters to uppercase in order to evaluate them uniformly
    boolean boolShape = shape.equals("TRIANGLE") || shape.equals("RECTANGLE") || shape.equals("CIRCLE"); //Boolean for if user inputed a valid shape
    
    //LOOP FOR IF USER DOESN'T INPUT A VALID SHAPE//
    while(!boolShape){ 
      System.out.print("Please Enter A Shape to Calculate Area - Rectangle, Triangle, or Circle: "); //Reprompt User for what kind of shape area they want to calculate
      shape = myScanner.nextLine(); //Accept User Input as a String for Type of Shape
      shape = shape.toUpperCase(); //Change all characters to uppercase in order to evaluate them uniformly
      boolShape = shape.equals("TRIANGLE") || shape.equals("RECTANGLE") || shape.equals("CIRCLE"); //Recalculate boolShape to see if user input is valid
      }
      
    //IF USER SELECTS TRIANGLE//
    if(shape.equals("TRIANGLE")){
      //System.out.print("Please Enter a Positive Number for Height of Triangle: ");
      double height = checkDimension("Please Enter a Positive Number for Height of Triangle: "); //Go to Method checkDimension
      //System.out.print("Please Enter a Positive Number for Base of Triangle: ");
      double base = checkDimension("Please Enter a Positive Number for Base of Triangle: "); //Go to Method checkDimension
      Area = triArea(height, base); //Go to Method for shape area using user inputs above
    }
    
    //IF USER SELECTS RECTANGLE//
    else if(shape.equals("RECTANGLE")){
      //System.out.print("Please Enter a Positive Number for Height of Rectangle: ");
      double height = checkDimension("Please Enter a Positive Number for Height of Rectangle: "); //Go to Method checkDimension
      System.out.print("Please Enter a Positive Number for Width of Rectangle: ");
      double width = checkDimension("Please Enter a Positive Number for Width of Rectangle: "); //Go to Method checkDimension
      Area = rectArea(height, width); //Go to Method for shape area using user inputs above
    }
    
    //IF USER SELECTS CIRCLE//
    else if(shape.equals("CIRCLE")){
      //System.out.print("Please Enter a Positive Number for the Radius of Circle: ");
      double radius = checkDimension("Please Enter a Positive Number for the Radius of Circle: "); //Go to Method checkDimension
      Area = cirArea(radius); //Go to Method for shape area using user inputs above
    }
    
    //CALCULATED AREA//
    System.out.println("The Area of Your Selected Shape = " + Area);
  }
  
  //METHOD FOR CHECKING IF USER INPUT FOR NUMBER IS VALID
  public static double checkDimension(String s){ //Method for Checking User Input
    //System.out.print(s); //Print string for what dimension value user is being prompted for inside () 
    double x = 0; //value for dimension
    Scanner myScanner = new Scanner(System.in); //Scanner for this method
    
    do{
      System.out.print(s); //Print string for what dimension value user is being prompted for inside () 
      while (!myScanner.hasNextDouble()){
        myScanner.nextLine(); // Clear Scanner if User Doesn't Input an Double
        System.out.print("Invalid Input, Please Input a Positive Number: "); //reprompt user for a valid number
      }
      x = myScanner.nextDouble(); //If User Enters a double
    } while (!(x > 0)); //loop is user doesn't input a positive number
    
    return x; //return the valid number to the variable of dimension
  }
  
  //METHOD FOR CALCULATING TRIANGLE AREA
  public static double triArea(double userHeight, double userWidth){
    return (userHeight * userWidth) / 2;
  }
  
  //METHOD FOR CALCULATING RECTANGLE AREA
  public static double rectArea(double userHeight, double userWidth){
    return (userHeight * userWidth);
  }
  
  //METHOD FOR CALCULATING CIRCLE AREA
  public static double cirArea(double userRadius){
    return Math.PI * userRadius * userRadius;
  }
}
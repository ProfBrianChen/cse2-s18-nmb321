///////////////////////////
///Nicole M. Bolton
///CSE 2 Homework 07
/*process a string by examining all the characters, or just a specified number of 
characters in the string, and determining if they are letters. 
One of the methods should accept just a string and the other should accept a string and an int.*/

import java.util.Scanner;

public class StringAnalysis{
  
  //MAIN METHOD//
  public static void main(String[] args){
    
    String userString = ""; //String for User Input
    String userDecision = ""; //String for User Decision
    int charNum = 0; //String for User Selected Character Value
    boolean stringAnalysis = false; //Boolean for StringAnalysis
    Scanner myScanner = new Scanner(System.in); //Scanner
    
    System.out.print("Enter a String: "); //Prompt User to Enter a String
    userString = myScanner.nextLine(); //Accept User Input for String and Save to userString
    userString = userString.toLowerCase(); //Change All Letters to Lowercase In Order to Evaluate
    
    System.out.print("Would You Like to Specify a Number of Characters to Evaluate? Yes or No: "); //Ask User if They Only Want to Evaluate a Certain Num of Characters
    userDecision = myScanner.nextLine(); //Accept User Input for String and Save to userDecision
    userDecision = userDecision.toUpperCase(); //Change All Letters to Uppercase In Order to Evaluate
    boolean boolUserDecision = userDecision.equals("YES") || userDecision.equals("NO"); //Boolean to Determine if User Entered a Valid Answer
    
    while(!boolUserDecision){ //Loop for if User Does Not Enter a Valid Anser
      System.out.print("Please Input 'Yes' or 'No': "); //Reprompt User to Enter Another Answer
      userDecision = myScanner.nextLine(); //Accept User Input for String and Save to userDecision
      userDecision = userDecision.toUpperCase(); //Change All Letters to Uppercase In Order to Evaluate
      boolUserDecision = userDecision.equals("YES") || userDecision.equals("NO"); //Reevaluate New userDecision
    }
    
    //IF USER DOES NOT WANT TO INPUT OWN VALUES
    if(userDecision.equals("NO")){
      stringAnalysis = Analysis(userString); //Go To Method Analysis that Accepts Only a String
    }
    
    //IF USER DOES NOT WANT TO INPUT OWN VALUES
    if(userDecision.equals("YES")){
      
      int length = userString.length(); //Calculate Length of String
      
      do{ //Loop to Determine if User Inputs a Valid Number
        System.out.print("Enter a Positive Number of Characters in String to Evaluate: "); //Prompt User for the Number of Characters to Evaluate
        
        while(!myScanner.hasNextInt()){ //If User Input is Not an Integer
          myScanner.nextLine(); //Clear Scanner
          System.out.print("Invalid Input, Please Input a Positive Number: "); //Prompt User to Input New Value
        }
        charNum = myScanner.nextInt(); //Save User Input to charNum
        
       while(charNum > length){ //If User Input is Greater Than the Number of Characters in String
          myScanner.nextLine(); //Clear Scanner
          System.out.print("Invalid Input, Please Input a Positive Number: "); //Prompt User to Input New Value
         
          while(!myScanner.hasNextInt()){ //Check if New User Input is an Integer
            myScanner.nextLine(); //Clear Scanner
            System.out.print("Invalid Input, Please Input a Positive Number: "); //Prompt User to Input New Value
          }
         
         charNum = myScanner.nextInt();//Save User Input to charNum
        }
        
      } while(!(charNum > 0)); //Make Sure Number is Positive
     
      stringAnalysis = Analysis(userString, charNum); //Go To Method Analysis That Accepts Both a String and an int
    }
    
    //IF ENTIRE ANALYZED STRING IS LETTERS
    if(stringAnalysis){
      System.out.println("String Consists of Only Letters");
    }
    
    //IF ENTIRE ANALYZED STRING IS NOT LETTERS
    if(!stringAnalysis){
      System.out.println("String Does Not Consist of Only Letters"); //Print
    }
  }
  
  //METHOD TO ANAYLZE ENTIRE STRING
  public static boolean Analysis(String s){
    
    int j = s.length(); //Length of String
    int charCntr = 0;//Counter for Characters That Are Not Letters
    
    for(int i = 0; i < j; i++){ //Checks Every Character Until End of String
      char currentChar = s.charAt(i); //Check Character in Position i
      if(currentChar >= 'a' && currentChar <= 'z'){ //If Character in i is Between a and z
        continue;
      }
      else{ //If Character in i is NOT Between a and z
        charCntr++; //Add 1 to Counter
      }
    }
    if(charCntr > 0){ //If Counter is Greater Than Zero, That Means There is a Character That is Not a Letter 
      return false; //Return False So Outside of Method it Says String is Not All Letters
    }
    else{ //If Counter is Zero, That Means All Characters Were Letters
      return true; //Return Tre So Outside of Method it Says String is All Letters
    }
  }
  
  //METHOD TO ANAYLZE SPECIFIED NUMBER OF CHARACTERS IN STRING
  public static boolean Analysis(String s, int j){
    
    int charCntr = 0; //Counter for Characters That Are Not Letters
    
    for(int i = 0; i < j; i++){ //Checks Every Character Until User Specification
      char currentChar = s.charAt(i); //Check Character in Position i
      if(currentChar >= 'a' && currentChar <= 'z'){ //If Character in i is Between a and z
        continue;
      }
      else{ //If Character in i is NOT Between a and z
        charCntr++; //Add 1 to Counter
      }
    }
    if(charCntr > 0){ //If Counter is Greater Than Zero, That Means There is a Character That is Not a Letter 
      return false; //Return False So Outside of Method it Says String is Not All Letters
    }
    else{ //If Counter is Zero, That Means All Characters Were Letters
      return true; //Return Tre So Outside of Method it Says String is All Letters
    }
  }
}
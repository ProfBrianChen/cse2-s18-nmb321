//////////////////////////
///// CSE 2 Lab06
///// Nicole M. Bolton
///// Encrypted X
///// Program will print out an X
//

import java.util.Scanner;

public class encrypted_x {
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    int input = 0; //Declare Variable for User Input
    int lowlim = 0;
    int uplim = 100;
    
    do{
      System.out.print("Please Enter Integer Between 0-100: "); //Prompt User for Integer Between 0 - 100
      while (!myScanner.hasNextInt()){
        myScanner.nextLine(); // Clear Scanner if User Doesn't Input an Integer
        System.out.print("Please Enter Integer Between 0-100: ");
      }
      input = myScanner.nextInt(); //If User Enters an Int
    } while (!(input >= lowlim && input <= uplim)); //Check if Input is Between Limits
    
    for(int i = 0; i < input; i ++){
      for(int j = 0; j < input; j++){
        if(j == i){
          System.out.print(' ');
        }
        else if(j == input - i - 1){
          System.out.print(' ');
        }
        else{
          System.out.print('*');
        }
      }
      System.out.println();
    }
  } 
}
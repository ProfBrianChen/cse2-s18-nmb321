//////////////////////////
///// CSE 2 Lab07
///// Nicole M. Bolton
///// Random Story Generator

import java.util.Random;
import java.util.Scanner;

public class lab07 {
  
  //MAIN METHOD//
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    /*String adj = "";
    String subj = "";
    String verb = "";
    String obj = "";*/
    
    String adj = adj();
    String subj = subj();
    String verb = verb();
    String obj = obj();
    
    System.out.println("The " + adj + subj + verb + "the " + obj + ".");
  }
  
  //METHOD FOR ADJECTIVE//
  public static String adj(){
    
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //(This statement generates random integers less than 10)
    
    String strAdjective = "";
    //Assign Random Generated Number to a Word
    switch (randomInt){
      case 0: strAdjective = "fancy ";
        break;
      case 1: strAdjective = "slow ";
        break;
      case 2: strAdjective = "fast ";
        break;
      case 3: strAdjective = "bitter ";
        break;
      case 4: strAdjective = "spicy ";
        break;
      case 5: strAdjective = "mean ";
        break;
      case 6: strAdjective = "slippery ";
        break;
      case 7: strAdjective = "petite ";
        break;
      case 8: strAdjective = "gigantic ";
        break;
      case 9: strAdjective = "fierce ";
        break;
    }
    return strAdjective;
   }
  //METHOD FOR SUBJECT OF SENTENCE
  public static String subj(){
    
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //(This statement generates random integers less than 10)
    
    String strSubject = "";
    //Assign Random Generated Number to a Word
    switch (randomInt){
      case 0: strSubject = "parrot ";
        break;
      case 1: strSubject = "dog ";
        break;
      case 2: strSubject = "cat ";
        break;
      case 3: strSubject = "goat ";
        break;
      case 4: strSubject = "whale ";
        break;
      case 5: strSubject = "squirrel ";
        break;
      case 6: strSubject = "caterpiller ";
        break;
      case 7: strSubject = "chicken ";
        break;
      case 8: strSubject = "lizard ";
        break;
      case 9: strSubject = "lobster ";
        break;
    }
    return strSubject;
  }
  
  //METHOD FOR PAST TENSE VERB//
  public static String verb(){
    
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //(This statement generates random integers less than 10)
    
    String strVerb = "";
    //Assign Random Generated Number to a Word
    switch (randomInt){
      case 0: strVerb = "chased ";
        break;
      case 1: strVerb = "got ";
        break;
      case 2: strVerb = "fought ";
        break;
      case 3: strVerb = "hid ";
        break;
      case 4: strVerb = "left ";
        break;
      case 5: strVerb = "threw ";
        break;
      case 6: strVerb = "froze ";
        break;
      case 7: strVerb = "found ";
        break;
      case 8: strVerb = "kept ";
        break;
      case 9: strVerb = "saw ";
        break;
    }
    return strVerb;
  }
  //METHOD FOR OBJECT NOUN//
  public static String obj(){
    
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //(This statement generates random integers less than 10)
    
    String strObject = "";
    //Assign Random Generated Number to a Word
    switch (randomInt){
      case 0: strObject= "banana ";
        break;
      case 1: strObject = "orange ";
        break;
      case 2: strObject = "apple ";
        break;
      case 3: strObject = "house ";
        break;
      case 4: strObject = "bike ";
        break;
      case 5: strObject = "plant ";
        break;
      case 6: strObject = "ball ";
        break;
      case 7: strObject = "flower ";
        break;
      case 8: strObject = "juice ";
        break;
      case 9: strObject = "fire ";
        break;
    }
    return strObject;
  }
}
///////////////////////////
///Nicole M. Bolton
///CSE 2 Homework 06
///Ask User for the Course Number, Department Name,
///Number of Times Meet, Start Time, Instructor Name, and Number of Students
//
import java.util.Scanner;

public class Hw05 {

  public static void main(String[] args){
  
    Scanner myScanner = new Scanner(System.in);
    
    //Initialize Variables for all Values
    int courseNumber = 0;
    String department = "";
    int timesMeet = 0;
    int startTime = 0;
    String instructor = "";
    int numStudents = 0;
    
    
    //COURSE NUMBER
    System.out.print("Enter Course Number: "); //Prompt User for a Course Number
    boolean boolCourseNum = myScanner.hasNextInt(); //Check if User Input is an Integer
    
    //If User Doesn't Input an Integer:
    //Keeps Looping Until User Inputs an Interger
    while (!boolCourseNum){
      System.out.print("Invalid Input, Please Input an Integer for Course Number: ");
      String junkWord = myScanner.next();
      boolCourseNum = myScanner.hasNextInt();
    }
    
    //If User Does Input an Integer
    if (boolCourseNum){
      courseNumber = myScanner.nextInt(); //Save User Input into Variable
    }
    
    
    //DEPARTMENT NAME
    String blank1 = myScanner.nextLine(); //Clear the Scanner
    System.out.print("Enter Department Name: "); //Prompt User for a Department Name for Course
    department = myScanner.nextLine();
    
    
    //NUMBER OF TIMES MEET
    System.out.print("Enter Number of Time Course Meets Per Week: ");
    boolean boolTimesMeet = myScanner.hasNextInt();
    
    //If User Doesn't Input an Integer:
    //Keeps Looping Until User Inputs an Interger
    while (!boolTimesMeet){
      System.out.print("Invalid Input, Please Input an Integer for Number of Time Course Meets Per Week: ");
      String junkWord = myScanner.next();
      boolTimesMeet = myScanner.hasNextInt();
    }
    
    //If User Does Input an Integer
    if (boolTimesMeet){
      timesMeet = myScanner.nextInt();
    }
    
    //START TIME
    System.out.print("Enter Time Course Starts: ");
    boolean boolStartTime = myScanner.hasNextInt();
    
    //If User Doesn't Input an Integer
    //Keeps Looping Until User Inputs an Integer
    while (!boolStartTime){
      System.out.print("Invalid Input, Please Input an Integer for Start Time(XXXX): ");
      String junkWord = myScanner.next();
      boolStartTime = myScanner.hasNextInt();
    }
    
    //If User Does Input a String
    if (boolStartTime){
      startTime = myScanner.nextInt();
    }
    
    //INSTRUCTOR NAME
    String blank2 = myScanner.nextLine(); //Clear the Scanner
    System.out.print("Enter Instructor Name: ");
    instructor = myScanner.nextLine();
    
    
    //NUMBER OF STUDENTS
    System.out.print("Enter Number of Student: ");
    boolean boolNumStudents = myScanner.hasNextInt();
    
    //If User Doesn't Input an Integer:
    //Keeps Looping Until User Inputs an Interger
    while (!boolNumStudents){
      System.out.print("Invalid Input, Please Input an Integer for Number of Students: ");
      String junkWord = myScanner.next();
      boolNumStudents = myScanner.hasNextInt();
    }
    
    //If User Does Input an Integer
    if (boolNumStudents){
      numStudents = myScanner.nextInt();
    }
    
    
    //CHECK THAT ALL VAULES FOT INPUTED CORRECTLY
    //System.out.println("Course Number: " + courseNumber);
    //System.out.println("Department Name: " + department);
    //System.out.println("Number of Times Per Week: " + timesMeet);
    //System.out.println("Start Time: " + startTime);
    //System.out.println("Instuctor: " + instructor);
    //System.out.println("Number of Students: " + numStudents);
   
  }
}
//////////////////////////
///// CSE 2 Lab08
///// Nicole M. Bolton

import java.util.Scanner;
import java.util.Random;

public class lab08{
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //Scanner
    Random rand  = new Random(); //Random Number Generator
    int randArray = rand.nextInt(6)+5; //Random Number Between 5 & 10
    
    String[] Students = new String[randArray]; //Array for Student Names
    int[] Grades = new int[randArray];
   
    
    System.out.println("Enter " + randArray + " Student Names: ");
    
    for (int i = 0; i < randArray; i ++){ //Loop to Input Student Name
      Students[i] = myScanner.nextLine();
    }
    
    for(int j = 0; j < randArray; j ++){
      Grades[j] = rand.nextInt(100);
    }
    
    System.out.println("Here are the Midterm Grades for the " + randArray + " Students Above: ");
    for(int k = 0; k < randArray; k++){
      System.out.println(Students[k] + ": " + Grades[k]);
    }
    
  }
}

///////////////////////////
/// Nicole M. Bolton
/// CSE 2 Homework 10
/// build a city, display it, invade it, display it, then update and display it in a loop 5 times.

import java.util.Random; //Random Number Generator

public class RobotCity{
  
  //MAIN METHOD//
  public static void main(String[] args){
    Random rand = new Random(); 
    
    int[][] city = buildCity(); //go to method build city, save in array "city"
    
    display(city); //go to method to print out the city
    
    int k = rand.nextInt(city.length*city[0].length); //generate random number of attackers; k can be any random number with the dimension

    invade(city, k); //go to method to "invade" city
    display(city); //go to method to display city post-invasion
    
    for(int i = 0; i < 5; i++){ //loop 5 times
      update(city); //update the city invasion as it moves east-wards
      display(city); //disply the updated city
    }
  }
  
  //METHOD TO GENERATE CITY//
  public static int[][] buildCity(){
    Random rand = new Random();
   
    int NS = rand.nextInt(5) + 10; //Dimension North to South (between 10 - 15)
    int EW = rand.nextInt(5) + 10;  //Dimension East to West (between 10 - 15)
    
    int city[][] = new int[NS][EW]; //array for city of random dimensions
    
    //Generate Population for Each Block//
    for(int i = 0; i < NS; i++){ //height North to South
      for(int j = 0; j < EW; j++){ //width East to West
        int population = rand.nextInt(899) + 100; //Random Population (between 100 - 999);
        city[i][j] = population; //assign random value to block
      }
    }
    return city; //return the generated city
  }
  
  
  //METHOD TO PRINT OUT CITY//
  public static void display(int[][] city){
    for(int i = 0; i < city.length; i++){ //NS Coordinate
      for( int j = 0; j < city[0].length; j++){ //EW Coordinate
        System.out.printf("%d  ", city[i][j]); //print out current block
      }
      System.out.println(); //new line
    }
    System.out.println(); //new line
  }
  
  //METHOD TO INVADE CITY
  public static void invade(int[][] city, int k){
    Random rand = new Random();
 
    int i = 0;
    while(i < k){ //loop to send each attacker to a different block
      int coordNS = rand.nextInt(city.length); //generate a random North-South coordinate
      int coordEW = rand.nextInt(city[0].length); //generate a random East-West coordinate
      if(city[coordNS][coordEW] < 0){ //if the random coordinate is zero, that means it has already been attacked
        continue; //loop again and create a dif random coordinate; don't increase value of i
      }
      city[coordNS][coordEW] = -city[coordNS][coordEW]; //if random coordinate hasn't already been attacked, attack it by making is negative
      i ++; //increase i by 1
      
    }
  }
  
  //METHOD TO UPDATE CITY//
  public static void update(int[][] city){
    
    for(int i = 0; i < city.length; i++){ //go through blocks starting from top to bottom
      for(int j = city[0].length - 1; j >= 0; j--){ //go through blocks right to left, this way it is easier to move the attackers to the right
        if(city[i][j] < 0){ //if current coordinate is positive, skip it; if current coordinate is negative, move the "attacker" to the left
          city[i][j] = -city[i][j]; //make current coordinate positive
          if(city[0].length - 1 == j){ //if j + 1 == length of width, at end of "city" therefore just continue
            continue;
          }
          else{ //if not at end of city
            city[i][j+1] = -city[i][j+1]; //make the value to the right of current coordinate positive
          }
        }
      }
    }
  }
}
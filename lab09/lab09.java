//////////////////////////
///// CSE 2 Lab09
///// Nicole M. Bolton

public class lab09{
  public static void main(String[] args){
    
    int[] array0 = new int[10];
    int[] array1;
    int[] array2;
    int[] array3;
    
    for(int i = 0; i < array0.length; i ++){
      array0[i] = i;
    }
    array1 = copy(array0);
    array2 = copy(array0);
    
    inverter(array0);
    print(array0);
    
    inverter2(array1);
    print(array1);
    
    array3 = inverter2(array2);
    print(array3);
    
  }
  
  public static int[] copy(int[] array){
    int[] newArray = new int[array.length];
    for(int i = 0; i < array.length; i++){
      newArray[i] = array[i];
    }
    return newArray;
  }
  public static void inverter(int[] array){
    
    for(int i = 0; i < array.length/2; i++ ){
      int temp = array[i];
      array[i] = array[array.length - 1 - i];
      array[array.length-1-i] = temp;
    }
    
  }
  public static int[] inverter2(int[] array){
    int j = 0;
    int[] temp = new int[array.length];
    for(int i = array.length - 1; i >= 0; i --){
      temp[j] = array[i];
      j++;
    }
    array = temp;
    return array;
    //print(array);
  }
  
  public static void print(int[] array){
    for(int i = 0; i < array.length; i ++){
      System.out.print(array[i] + " ");
    }
    System.out.println();
  }
  
}
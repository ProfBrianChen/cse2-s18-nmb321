//////////////////////////
///// CSE 2 Lab02
///// Nicole Bolton
/// My Bicylce Cyclometer
/// Given Time and Rotation,
/// Print Number of Minutes for Each Trip
/// Print Number of Counts for Each Trip
/// Print the Distance of Each Trip in Miles
/// Print the Distance of The Two Trips Combined
//
public class Cyclometer{
    // main method required for every Java program
    public static void main(String [] args){
      int secsTrip1 = 480; // Time of Trip One in Seconds
      int secsTrip2 = 3220; // Time of Trip Two in Seconds
      int countsTrip1 = 1561; // Count of Trip One
      int countsTrip2 = 9037; // Count of Trip Two
      
      double wheelDiameter = 27.0, // Diameter of Bike Wheel
      PI = 3.14159, // Value of Pi
      feetPerMile = 5280, // Conversion Mile to Feet
      inchesPerFoot = 12, // Conversion Feet to Inches
      secondsPerMinute = 60; // Conversion Minute to Seconds
      double distanceTrip1, distanceTrip2, totalDistance; // Distances of Trip One, Trip Two, Total Distances in inches
      
      // Print out Calculated Times
      System.out.println("Trip 1 took " + 
                         (secsTrip1/secondsPerMinute) + " minutes and had " + 
                         countsTrip1 + " counts.");
      System.out.println("Trip 1 took " + 
                         (secsTrip2/secondsPerMinute) + " minutes and had " + 
                         countsTrip2 + " counts.");
      
      // Calculations for Distance in Inches
      distanceTrip1 = countsTrip1 * wheelDiameter * PI;
      // Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI) 
      distanceTrip1 /= inchesPerFoot * feetPerMile; // Distance in Miles
      distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // Distance Trip 2 in Miles
      totalDistance = distanceTrip1 + distanceTrip2;
      
      // Print out Calculated Distances
      System.out.println("Trip 1 was " + distanceTrip1 + " miles");
      System.out.println("Trip 2 was " + distanceTrip2 + " miles");
      System.out.println("The total distance was " + totalDistance + " miles");
      
      
    } //end of main method
} // end of class
///////////////////////////
///Nicole M. Bolton
///CSE 2 Homework 08 Program 2 - RemoveElements
/*complete the undefined methods*/

import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  
  //GIVEN CODE//
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.println("Random input 10 ints [0-9]");
  	num = randomInput(num); //RANDOMINPUT METHOD
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  
  //METHOD RANDOM INT//
  public static int[] randomInput(int[] x){
    
    Random rand = new Random(); //random generator
    for(int i = 0; i < x.length; i ++){ //loop for all indexes in array
      x[i] = rand.nextInt(9); //generate a number between 0 and 9
    }
    return x; //return array x 
  }
  
  
  //METHOD TO REMOVE AN ELEMENT//
  public static int[] delete(int[] list, int pos){
    
    if(pos > list.length){ //If User input is greater than the array, input is invalid
      System.out.println("The index is not valid");
      return list; //return the array withone removing anything
    }
    
    for(int i = pos; i < list.length - 1; i++){ //Start loop at position you want to subtract, then go through rest of list length 
      list[i] = list[i + 1]; //replace every element after postion to delete with the postion that was after it
    }
    return list; //return the array with deleted index
  }
  
  public static int[] remove(int[] list, int target){
    boolean targetRem = false;
    int x = list.length; //to keep track of what length the new array should have

    for(int i = 0; i < list.length; i ++){ //go through the array and determine if any value in the array matches user input
      if(list[i] == target){ //if an element matched user input
        targetRem = true; //return true
        x--; //make the size of the new array minus one
      }
      else{continue;}
    }
    
    if(targetRem){ //If found an element in array that matched user input
      System.out.println("Element " + target + " has been found");
    }
    else if(!targetRem){ //If didn't find an element in array that matched user inpu
      System.out.println("Element " + target + " was not found");
      return list; //leave method with an unchanged list
    }
    
    int[] Array = new int[x]; //make a new array to put elements in
    int k = 0;
    
    for(int i = 0; i < list.length; i++){ //loop through old array
      if(list[i] != target){ //if element in array does NOT match the user "target" value, add it to the new array
        Array[k] = list[i];
        k++; //next value in the new array
      }
    }
    return Array; //return the new array
  }
}



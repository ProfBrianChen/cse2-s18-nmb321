///////////////////////////
///Nicole M. Bolton
///CSE 2 Homework 08 Program 1 - CSE2Linear
/*prompts the user to enter 15 ints for students final grades in CSE2.
 Check that the user only enters ints 
 prompt the user to enter a grade to be searched for. 
 Use binary search to find the entered grade. 
 Indicate if the grade was found or not, and print out the number of iterations used.
 Scramble the grades and then preform linear search again*/

import java.util.Scanner;

public class CSE2Linear{
  
  
  
  //MAIN METHOD//
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);  //Scanner
    int[] finalGrades = new int[15]; //Array with 15 Spaces 
    
    System.out.println("Enter 15 Ascending Ints for Final Grades in CSE2:"); //Prompt User to Enter 15 Ascending Numbers
    
    int userNum = 0; //Variable for User Input
    
   //LOOP FOR PROMPTING A USER INPUT FOR ARRAY// 
    for(int i = 0; i < finalGrades.length; i ++){
      
      //LOOP FOR CHECKING THAT USER INPUTS A VALID NUMBER//
      do{
				
				
        if(!myScanner.hasNextInt()){ //Check if User Inputs an Int
          myScanner.next(); //Clear the Scanner
          System.out.println("Error. Number Must Be an Int."); //Error Message
          continue; //If User Dose Not Input an Int, Loop Again
        }
       
        userNum = myScanner.nextInt(); //Save User Input into Temporary Variable userNum
        
        if(userNum < 0 || userNum > 100){ //Check if User Input is in Range
          System.out.println("Error. Number Must Be Between 0 and 100."); //Error Message
          continue; //If User Dose Not Input an Int in Range, Loop Again
        }
        
        finalGrades[i] = userNum; //If User Input is an Int and in Range, Save User Input into the Array at Place i
        
        if(i == 0){ //If in place 0, There is no number to compare the value to so just break out of loop and continue to next value
            break; 
        }
        else if(finalGrades[i-1] > finalGrades[i]){ //If User Input for Current Number is Not Greater Than the Previous One 
          System.out.println("Error. Numbers is Not Greater Than or Equal to Last Number."); //Error Message
        } 
      }while(i == 0 || finalGrades[i-1] > finalGrades[i]); //Keep Looping While User Value is Not Greater than Previous Input or if i was 0 and user input wasn't correct
    }
    
    printArray(finalGrades);
    
		int numSearch = 0; 
    int userGrade = 0;
    int[] Scramble;
    
    System.out.println(); //New Line
    userGrade = userSelect(numSearch);
    binarySearch(finalGrades, userGrade); //Run Method linearSearch
    System.out.println(); //New Line
    Scramble = Scrambled(finalGrades); //Scramble finalGrades and put it into a new array
    printArray(Scramble);
    System.out.println(); //New Line
    userGrade = userSelect(numSearch);
    linearSearch(Scramble, userGrade); //Run Method linearSearch for scramble
    System.out.println(); ///New Line
  }

  
  
  //METHOD FOR PROMPTING USER FOR A GRADE TO SEARCH FOR//
  public static int userSelect(int x){
    
    Scanner myScanner = new Scanner(System.in);
    
    //CHECK FOR USER INPUT IS VALID//
    do{
			System.out.print("Enter a Grade to Search For: "); //Prompt User to Enter a Grade to Search for
      if(!myScanner.hasNextInt()){ //Check if User Inputs an Int
        myScanner.next(); //Clear Scanner
        System.out.println("Error. Input Must Be an Int."); //Error Message
          continue; //If User Dose Not Input an Int, Loop Again
        }
      
      x = myScanner.nextInt();
      
      if(x < 0 || x > 100){
        System.out.println("Error. Input Must Be Between 0 and 100."); //Error Message
      }
    }while(x < 0 || x > 100); //If User Value is Not in Range
    
    return x;
  } 
  
  
  
  
  //METHOD FOR BINARY SEARCH//
  public static void binarySearch(int[] x, int userSearch){
		int low = 0; //bottom range
		int high = x.length - 1; //top range
    int iterations = 0; //iterateions
    
    while(low <= high){
			iterations ++; //keep track of iterations
			int mid = ( low + (high - low) /2);
			
			if (userSearch > x[mid]){
				low = mid + 1; //update bottom range
			}
			
			else if (userSearch < x[mid]){
				high = mid - 1; //update top range
			}
			else if( x[mid] == userSearch){
				System.out.print(userSearch + " Was Found in List With " + iterations + " Iterations"); //if find number in array
				return;
			}
    }
		System.out.print(userSearch + " Was Not Found in List After " + iterations + " Iterations"); //if don't find number in array
		return;
	}

  
  
  
  //METHOD FOR LINEAR SEARCH//
  public static void linearSearch(int[] x, int userSearch){
   
    boolean arraySearch = false; //boolean for if value is in the array
    int iterations = 0; //counter for number of iterations
    int i = 0; //iterator variable
    
    //LOOP TO SEARCH FOR USER VALUE IN ARRAY//
    while(i < x.length){
      iterations ++; //increase the iteration counter
      
      if(x[i] == userSearch){ //If value in array place i is equal to user input, found user input value in array
        arraySearch = true; //change arraySearch boolean to true because found the value
        break; //no need to check further, therefore break out of loop
      }
      else if(x[i] != userSearch){ //If value in array place i is not equal to user input, need to examine next place
        arraySearch = false; //keep arraySearch boolean false because haven't found the value
        i++; //add i+1 so that the loop checks the next value
      }
    }
    
    //PRINT STATEMENTS FOR IF USERINPUT WAS FOUND OR NOT//
    if(arraySearch == true){ //if arraySearch is true then that means userinput was in array, therefore print the following
      System.out.print(userSearch + " Was Found in List With " + iterations + " Iterations");
      return;
    }
    else if(arraySearch == false){ //if arraySearch is flase then that means userinput was not in array, therefore print the following
      System.out.print(userSearch + " Was Not Found in List After " + iterations + " Iterations");
      return;
    }  
  }
 
  
  //METHOD TO SCRAMBLE ARRAY//
  public static int[] Scrambled(int[] x){
    
    for (int i = 0; i < x.length; i++) {
	  //find a random member to swap with
      int target = (int) (x.length * Math.random() );
      //swap the values
	    int temp = x[target];
	    x[target] = x[i];
	    x[i] = temp;
    }
    return x;
  }
  
  
  //METHOD TO PRINT ARRAY//
  public static void printArray(int[] x){
    for(int i = 0; i < x.length; i++){ //loop for printing out array
      System.out.print(x[i] + " ");
    }
    return;
  }
  
}

//////////////////////////
///// CSE 2 Lab05
///// Nicole M. Bolton
///// Twist Generator
///// Program will print out a simple "twist" on the screen
//
import java.util.Scanner;

public class TwistGenerator {
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //Name Scanner
    int length = -1; //initialize Integer Length of Twist
    boolean val = true; //initialize Boolean for Val
    System.out.print("Input Positive Integer: "); //Prompt user for a positive integer
    myScanner.hasNextInt(); //Check that User Inputed an Int
    if(myScanner.hasNextInt()){ //myScanner.hasNextInt() will be true if user inputs an Int
      length = myScanner.nextInt(); //If User Input was a Int, save value for length
    }
    
    //Check if Length was Positive
    if (length < 0){ 
      val = false; //If Length was not Positive, make val = false
      }
    //So Long that User Doesn't Input a Positive Int, Continue to Ask for One
    while(!val){ 
      System.out.println("Invalid Number");
      System.out.print("Input Positive Integer: ");
       
      length = myScanner.nextInt(); //Take User Input for Corrected Value
      
      //Check if Length was Positive
      if (length < 0){ 
         val = false; //Continue to Make val false if User Doesn't Input a Positive Int
      } 
        
       if (length > 0){
         val = true; //When User Inputs a Positive Int, They Will Be Kicked Out of the Loop
       }
      }
    
    //When User Inputs a Valid Number, Create Twist
    if (val){ 
      int i = 1; //Iterator Variable for First Line of Twist
      int j = 1; //Iterator Variable for Second Line of Twist 
      int k = 1; //Iterator Variable for Third Line of Twist
      
      //For the Twist, It Repeats Every 3 Places
      //Therefore, Divide i by 3 and the Remainder will be Where the Twist Element Goes
      //Loop For First Line
      while(length >= i){ //Keep Looping if i <= length
          if ((i % 3) == 0){ 
            System.out.print('/');
          }
          else if((i % 3) == 1){
            System.out.print('\\');
          }
          else if((i % 3) == 2){
            System.out.print(' ');
          }
        i = i + 1;
        } 
      
      System.out.println();//Next Line of Twist
      
      //Loop For Second Line
      while(length >= j){ //Keep Looping if j <= length
          if ((j % 3) == 2){
            System.out.print('X');
          }
          else{
            System.out.print(' ');
          }
        j = j + 1;
        }
      
       System.out.println(); //Next Line of Twist
     
      //Loop for Third Line 
     while(length >= k){ //Keep Looping if k <= length
          if ((k % 3) == 0){
            System.out.print('\\');
          }
          else if((k % 3) == 1){
            System.out.print('/');
          }
          else if((k % 3) == 2){
            System.out.print(' ');
          }
        k = k + 1;
        }
      
        System.out.println();
      
      } 
    
    //If User Doesn't Input an Int, Clear the Scanner
    else{
      String junkWord = myScanner.next();
    }
  }
}
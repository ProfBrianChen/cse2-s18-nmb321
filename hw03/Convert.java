///////////////////////////
///Nicole M. Bolton
///CSE 2 Homework 03 Convert
///Based on user input, convert acres and amount of rain into cubic miles affected
//
import java.util.Scanner;
//
//
public class Convert{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the affected area in acres: "); //Prompt user to input for area affected my rain fall in acres
    double affectedAreaAcres = myScanner.nextDouble(); //Accept user input for area
    
    System.out.print("Enter the rainfall in the affected area: "); //Prompt user for the quantity of rainfall in inches
    double rainfallInches = myScanner.nextDouble();
    
    double affectedAreaMiles, rainfallMiles, quantityofRain; //Calculated Values
    
    double squareMilesPerAcre = 0.0015625; //Conversion from acres to square miles
    double milesPerInch = 1.57828e-5; //Conversion from inches to milesPerInch
    
    affectedAreaMiles = affectedAreaAcres * squareMilesPerAcre; //Convert Acres affected to Square miles
    rainfallMiles = rainfallInches * milesPerInch; //Convert inches of rain to milesPerInch
    
    quantityofRain = affectedAreaMiles * rainfallMiles; //Total quantity of rain in cubic miles
    System.out.println(quantityofRain + " cubic miles");
    
    
  }
}
//////////////////////////
///Nicole M. Bolton
///CSE 2 Homework 03 Pyramid
///Prompt user for dimentions of a pyramid and return the volume
//
import java.util.Scanner;

public class Pyramid{
  public static void main(String[] args){
  
    Scanner myScanner = new Scanner(System.in);
    System.out.print("The square side of the pyramid is (input length): "); //Prompt user input for square side of pyramid
    double squareSide = myScanner.nextDouble(); //Accept user input for square side
    
    System.out.print("The height of the pyramid is (input height): "); //Prompt user input for height of pyramid
    double height = myScanner.nextDouble(); //Accept user input for height
    
    //Equation of Square Pyramid V = 1/3 a^2 h
    double squareSideSquared = Math.pow(squareSide, 2.0); //square the side length
    double volume = (squareSideSquared * height / 3); //Calculated value for volume
    System.out.println("The volume inside the pyramid is: " + volume);
    
    
    
  }
  
}
/////////////////////
///////////////////////////////////
///CSE 2 Homework 01 WelcomeClass
///
 public class WelcomeClass {
 
   public static void main(String[] args) {
     // Prints Welcome & Student User into to the terminal window.
     System.out.println("-----------\n| WELCOME |\n-----------\n ^  ^  ^  ^  ^  ^\n/ \\/ \\/ \\/ \\/ \\/ \\\n<-N--M--B--3--2--1->\n\\ /\\ /\\ /\\ /\\ /\\ / \n v  v  v  v  v  v ");
   }
}
//////////////////////////
///Nicole M. Bolton
///CSE 2 Homework 02 Arithmetic
///Caluculates Cost of Item and Total Cost With and Without Tax
//

public class Arithmetic{
  
    public static void main(String [] args){
      //Articles of Clothing
      int numPants = 3; //Number of Pairs of Pants
      double pantsPrice = 34.98; //Cost Per Pair of Pants
      
      int numShirts = 2; //Number of Sweatshirtsdouble
      double shirtPrice = 24.99; //Cost Per Shirt
      
      int numBelts = 1; //Number of Belts
      double beltPrice = 33.99; //Cost Per Box of Belts
      
      //Sales Tax
      double paSalesTax = 0.06;
      
      //Calculated Values of Total Costs
      double totalPants, totalShirts, totalBelts, totalCostBeforeTax, totalCost;
      
      //Calculated Values of Total Costs With Sales Costs
      double taxPants, taxShirts, taxBelts, totalSalesTax;
      
      //Calculated Cost of Item Without Tax
      totalPants = ((int) (100 * (numPants * pantsPrice))) / 100.0;
      totalShirts = ((int) (100 * (numShirts * shirtPrice))) / 100.0;
      totalBelts = ((int) (100 * (numBelts * beltPrice))) / 100.0;
      
      System.out.println("Total Cost of Pants: $" + (totalPants));
      System.out.println("Total Cost of Shirts: $" + (totalShirts));
      System.out.println("Total Cost of Belts: $" + (totalBelts));
      
      //Calculated Item Sales Tax
      taxPants = ((int) (100 * (totalPants * paSalesTax))) / 100.0;
      taxShirts = ((int) (100 * (totalShirts * paSalesTax))) / 100.0;
      taxBelts = ((int) (100 * (totalBelts * paSalesTax))) / 100.0;
      
      System.out.println("Sales Tax Charged on Pants: $" + (taxPants));
      System.out.println("Sales Tax Charged on Shirts: $" + (taxShirts));
      System.out.println("Sales Tax Charged on Belts: $" + (taxBelts));
      
      //Calculated Total Without Tax
      totalCostBeforeTax = totalPants + totalShirts + totalBelts;
      System.out.println("Total Cost of Purchases Without Tax: $" + totalCostBeforeTax);
      
      //Calculated Total Sales Tax
      totalSalesTax = taxPants + taxShirts + taxBelts;
      System.out.println("Total Sales Tax on Items: $" + totalSalesTax);
      
      //Calculated Total Cost With Tax
      totalCost = totalCostBeforeTax + totalSalesTax;
      System.out.println("Total Cost of Purchases Plus Tax: $" + totalCost);


    }
}
